import Domain.*;

import static org.junit.jupiter.api.Assertions.*;

class LogicTests {

    @org.junit.jupiter.api.Test
    void SuccessCalculateTotalBacklogItemsAndFindDeveloper() {
        Person testDeveloper = new Person("Test", "test@gmail.com", Role.JUNIOR, "0612345678", "testSlack");
        Person scrumMaster = new Person("Scrummaster", "scrummaster@gmail.com", Role.SCRUMMASTER, "0611114460", "scrumMaster01");

        ProductBacklog backlog = new ProductBacklog("AvansDevOps", scrumMaster);
        SprintBacklog sprintBacklog = new SprintBacklog(backlog, "Sprint 1", scrumMaster);

        BacklogItem item = new BacklogItem(sprintBacklog, "Authentication");
        BacklogItem item2 = new BacklogItem(sprintBacklog, "Authentication2");

        testDeveloper.addBacklogItem(item);
        testDeveloper.addBacklogItem(item2);

        item.setDeveloper(testDeveloper);

        assertEquals(2, sprintBacklog.getBacklogItems().size());
        assertEquals(sprintBacklog.getBacklogItems().get(0).getDeveloper(), testDeveloper);
    }

    @org.junit.jupiter.api.Test
    void SuccessCalculateTotalActivitiesAndFindDeveloper() {
        Person testDeveloper = new Person("Test", "test@gmail.com", Role.JUNIOR, "0612345678", "testSlack");
        Person scrumMaster = new Person("Scrummaster", "scrummaster@gmail.com", Role.SCRUMMASTER, "0611114460", "scrumMaster01");

        ProductBacklog backlog = new ProductBacklog("AvansDevOps", scrumMaster);
        SprintBacklog sprintBacklog = new SprintBacklog(backlog, "Sprint 1", scrumMaster);

        BacklogItem item = new BacklogItem(sprintBacklog, "Authentication");

        testDeveloper.addBacklogItem(item);

        item.setDeveloper(testDeveloper);

        Activity activity1 = new Activity(item, "Login");
        Activity activity2 = new Activity(item, "Register");
        Activity activity3 = new Activity(item, "Logout");

        testDeveloper.addActivity(activity1);
        testDeveloper.addActivity(activity2);
        testDeveloper.addActivity(activity3);

        activity1.setDeveloper(testDeveloper);

        assertEquals(3, sprintBacklog.getBacklogItems().get(0).getActivities().size());
        assertEquals(testDeveloper, sprintBacklog.getBacklogItems().get(0).getActivities().get(0).getDeveloper());
    }

    @org.junit.jupiter.api.Test
    void SuccessAddItemsToBacklog() {
        Person testDeveloper = new Person("Test", "test@gmail.com", Role.LEAD, "0612345678", "testSlack");
        Person scrumMaster = new Person("Scrummaster", "scrummaster@gmail.com", Role.SCRUMMASTER, "0611114460", "scrumMaster01");

        ProductBacklog backlog = new ProductBacklog("AvansDevOps", scrumMaster);
        SprintBacklog sprintBacklog = new SprintBacklog(backlog, "Sprint 1", scrumMaster);

        BacklogItem item = new BacklogItem(sprintBacklog, "Authentication");
        BacklogItem item2 = new BacklogItem(sprintBacklog, "Authentication2");

        testDeveloper.addBacklogItem(item);
        testDeveloper.addBacklogItem(item2);

        item.setDeveloper(testDeveloper);

        Forum forum = new Forum("Bedrijfsforum");
        Discussion discussion = new Discussion("Auth", item, forum);
        item.addDiscussion(discussion);

        Reaction reaction = new Reaction("Ik snap er niks van.", discussion);
        discussion.addReaction(reaction);
        discussion.addReaction(new Reaction("Ik snap het wel.", discussion));

        assertEquals(discussion.getReactions().size(), 2);
        assertEquals("Auth", discussion.getSubject());
        assertEquals("Ik snap er niks van.", reaction.getDescription());
    }

    @org.junit.jupiter.api.Test
    void CannotFinishSprintWithOpenItems() {
        Person leadDeveloper = new Person("Test", "test@gmail.com", Role.LEAD, "0612345678", "testSlack");
        Person scrumMaster = new Person("Scrummaster", "scrummaster@gmail.com", Role.SCRUMMASTER, "0611114460", "scrumMaster01");

        ProductBacklog backlog = new ProductBacklog("AvansDevOps", scrumMaster);
        SprintBacklog sprintBacklog = new SprintBacklog(backlog, "Sprint 1", scrumMaster);

        BacklogItem item = new BacklogItem(sprintBacklog, "Authentication");
        BacklogItem item2 = new BacklogItem(sprintBacklog, "Authentication2");

        leadDeveloper.addBacklogItem(item);
        leadDeveloper.addBacklogItem(item2);

        sprintBacklog.setDone();
        assertFalse(sprintBacklog.getDone());
    }

    @org.junit.jupiter.api.Test
    void CanFinishSprintWithoutOpenItems() {
        Person leadDeveloper = new Person("Test", "test@gmail.com", Role.LEAD, "0612345678", "testSlack");
        Person scrumMaster = new Person("Scrummaster", "scrummaster@gmail.com", Role.SCRUMMASTER, "0611114460", "scrumMaster01");

        ProductBacklog backlog = new ProductBacklog("AvansDevOps", scrumMaster);
        SprintBacklog sprintBacklog = new SprintBacklog(backlog, "Sprint 1", scrumMaster);

        BacklogItem item = new BacklogItem(sprintBacklog, "Authentication");
        BacklogItem item2 = new BacklogItem(sprintBacklog, "Authentication2");
        leadDeveloper.setItemDone(item);
        leadDeveloper.setItemDone(item2);

        sprintBacklog.setDone();
        assertTrue(sprintBacklog.getDone());
    }
}
