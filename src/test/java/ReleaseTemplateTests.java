import Domain.*;
import ReleaseTemplate.MainRelease;
import ReleaseTemplate.Release;

import static org.junit.jupiter.api.Assertions.*;

class ReleaseTemplateTests {
    //#1
    @org.junit.jupiter.api.Test
    void SuccessRunningTemplateRelease() {
        Person testDeveloper = new Person("Test", "test@gmail.com", Role.JUNIOR, "0612345678", "testSlack");
        Person scrumMaster = new Person("Scrummaster", "scrummaster@gmail.com", Role.SCRUMMASTER, "0611114460", "scrumMaster01");

        Person productOwnerJan = new Person("Jan", "productownerjan@gmail.com", Role.PRODUCTOWNER, "0612121212", "none");

        ProductBacklog backlog = new ProductBacklog("AvansDevOps", productOwnerJan);
        SprintBacklog sprintBacklog = new SprintBacklog(backlog, "Sprint 1", scrumMaster);

        BacklogItem item1 = new BacklogItem(sprintBacklog, "Authentication1");
        BacklogItem item2 = new BacklogItem(sprintBacklog, "Authentication2");


        item1.setDeveloper(testDeveloper);
        item2.setDeveloper(testDeveloper);

        sprintBacklog.addBacklogItems(item1);
        sprintBacklog.addBacklogItems(item2);

        Release release = new MainRelease(sprintBacklog);
        release.release();

        assertTrue(release.isFinishedSuccessful());
    }

    @org.junit.jupiter.api.Test
    void FailRunningTemplateRelease() {
        Person testDeveloper = new Person("Test", "test@gmail.com", Role.JUNIOR, "0612345678", "testSlack");
        Person scrumMaster = new Person("Scrummaster", "scrummaster@gmail.com", Role.SCRUMMASTER, "0611114460", "scrumMaster01");

        Person productOwnerJan = new Person("Jan", "productownerjan@gmail.com", Role.PRODUCTOWNER, "0612121212", "none");

        ProductBacklog backlog = new ProductBacklog("AvansDevOps", productOwnerJan);
        SprintBacklog sprintBacklog = new SprintBacklog(backlog, "Sprint 1", scrumMaster);

        BacklogItem item1 = new BacklogItem(sprintBacklog, "Authentication1");
        BacklogItem item2 = new BacklogItem(sprintBacklog, "Authentication2");


        item1.setDeveloper(testDeveloper);
        item2.setDeveloper(testDeveloper);

        sprintBacklog.addBacklogItems(item1);
        sprintBacklog.addBacklogItems(item2);

        Release release = new MainRelease(sprintBacklog);

        assertFalse(release.isFinishedSuccessful());
    }
}
