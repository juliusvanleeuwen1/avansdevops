import Domain.*;
import ReportBuilder.*;

import java.text.DecimalFormat;

import static org.junit.jupiter.api.Assertions.*;

class ReportBuilderTests {
    //#1
    private static final DecimalFormat df = new DecimalFormat("0.0");

    @org.junit.jupiter.api.Test
    void SuccessBuildReportWithHeaderAndFooterAndEffortPoints() {
        Person julius = new Person("Julius", "vanjulius@gmail.com", Role.JUNIOR, "0611114460", "juliusvanleeuwen1");
        Person martijn = new Person("Martijn", "martijn@gmail.com", Role.SENIOR, "0613114460", "martijnlaffort");
        Person piet = new Person("Piet", "piet@gmail.com", Role.JUNIOR, "0612114460", "martijnlaffort");

        Person productOwnerJan = new Person("Jan", "productownerjan@gmail.com", Role.PRODUCTOWNER, "0612121212", "none");

        ProductBacklog backlog = new ProductBacklog("AvansDevOps", productOwnerJan);
        SprintBacklog sprintBacklog = new SprintBacklog(backlog, "Sprint 1", julius);

        BacklogItem item1 = new BacklogItem(sprintBacklog, "Authentication1");
        BacklogItem item2 = new BacklogItem(sprintBacklog, "Authentication2");
        BacklogItem item3 = new BacklogItem(sprintBacklog, "Authentication3");


        item1.setDeveloper(julius);
        item2.setDeveloper(martijn);
        item3.setDeveloper(piet);

        sprintBacklog.addBacklogItems(item1);
        sprintBacklog.addBacklogItems(item2);
        sprintBacklog.addBacklogItems(item3);

        ReportBuilder reportBuilder = new ReportWithBothBuilder(sprintBacklog);
        Director director = new Director(reportBuilder);

        director.createReport();

        Report newReport = director.getReport();

        newReport.generateEffortPointsReport();

        assertTrue(newReport.showHeader);
        assertTrue(newReport.showFooter);
        assertEquals("5.6", df.format(newReport.effortPoints));
    }

    @org.junit.jupiter.api.Test
    void SuccessBuildReportWithOutHeaderAndFooterAndWrongEffortPoints() {
        Person julius = new Person("Julius", "vanjulius@gmail.com", Role.JUNIOR, "0611114460", "juliusvanleeuwen1");
        Person martijn = new Person("Martijn", "martijn@gmail.com", Role.SENIOR, "0613114460", "martijnlaffort");
        Person piet = new Person("Piet", "piet@gmail.com", Role.JUNIOR, "0612114460", "martijnlaffort");

        Person productOwnerJan = new Person("Jan", "productownerjan@gmail.com", Role.PRODUCTOWNER, "0612121212", "none");

        ProductBacklog backlog = new ProductBacklog("AvansDevOps", productOwnerJan);
        SprintBacklog sprintBacklog = new SprintBacklog(backlog, "Sprint 1", julius);

        BacklogItem item1 = new BacklogItem(sprintBacklog, "Authentication1");
        BacklogItem item2 = new BacklogItem(sprintBacklog, "Authentication2");
        BacklogItem item3 = new BacklogItem(sprintBacklog, "Authentication3");


        item1.setDeveloper(julius);
        item2.setDeveloper(martijn);
        item3.setDeveloper(piet);

        sprintBacklog.addBacklogItems(item1);
        sprintBacklog.addBacklogItems(item2);
        sprintBacklog.addBacklogItems(item3);

        ReportBuilder reportBuilder = new ReportWithNoneBuilder(sprintBacklog);
        Director director = new Director(reportBuilder);

        director.createReport();

        Report newReport = director.getReport();

        newReport.generateEffortPointsReport();

        assertFalse(newReport.showHeader);
        assertFalse(newReport.showFooter);
        assertNotEquals("2,9", df.format(newReport.effortPoints));
    }
}
