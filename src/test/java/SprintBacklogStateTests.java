import Domain.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class SprintBacklogStateTests {
    //#1
    @org.junit.jupiter.api.Test
    void SuccessGetDoingStateFromBacklogItem() {
        Person testDeveloper = new Person("Test", "test@gmail.com", Role.JUNIOR, "0612345678", "testSlack");
        Person scrumMaster = new Person("Scrummaster", "scrummaster@gmail.com", Role.SCRUMMASTER, "0611114460", "scrumMaster01");

        ProductBacklog backlog = new ProductBacklog("AvansDevOps", scrumMaster);
        SprintBacklog sprintBacklog = new SprintBacklog(backlog, "Sprint 1", scrumMaster);

        sprintBacklog.setState(sprintBacklog.getCreatedState());
        sprintBacklog.setState(sprintBacklog.getInDevelopmentState());
        sprintBacklog.setState(sprintBacklog.getReviewedState());
        sprintBacklog.setState(sprintBacklog.getFinishedState());

        assertEquals(sprintBacklog.getFinishedState(), sprintBacklog.getState());
    }
    @org.junit.jupiter.api.Test
    void FailGetTestingStateFromBacklogItem() {
        Person testDeveloper = new Person("Test", "test@gmail.com", Role.JUNIOR, "0612345678", "testSlack");
        Person scrumMaster = new Person("Scrummaster", "scrummaster@gmail.com", Role.SCRUMMASTER, "0611114460", "scrumMaster01");

        ProductBacklog backlog = new ProductBacklog("AvansDevOps", scrumMaster);
        SprintBacklog sprintBacklog = new SprintBacklog(backlog, "Sprint 1", scrumMaster);

        sprintBacklog.setState(sprintBacklog.getCreatedState());
        sprintBacklog.setState(sprintBacklog.getInDevelopmentState());
        sprintBacklog.setState(sprintBacklog.getReviewedState());

        assertNotEquals(sprintBacklog.getFinishedState(), sprintBacklog.getState());
    }
}
