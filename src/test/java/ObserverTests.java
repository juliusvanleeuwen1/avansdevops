import Domain.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class ObserverTests {
    //#1
    @org.junit.jupiter.api.Test
    void SuccessBacklogItemHasObserver() {
        Person testDeveloper = new Person("Test", "test@gmail.com", Role.JUNIOR, "0612345678", "testSlack");
        Person scrumMaster = new Person("Scrummaster", "scrummaster@gmail.com", Role.SCRUMMASTER, "0611114460", "scrumMaster01");

        ProductBacklog backlog = new ProductBacklog("AvansDevOps", scrumMaster);
        SprintBacklog sprintBacklog = new SprintBacklog(backlog, "Sprint 1", scrumMaster);

        BacklogItem item = new BacklogItem(sprintBacklog, "Authentication");

        item.addObserver(testDeveloper);
        item.notifyObservers(item);

        BacklogItem item2 = new BacklogItem(sprintBacklog, "Item");
        item2.addObserver(testDeveloper);
        item2.addObserver(scrumMaster);
        testDeveloper.addBacklogItem(item2);

        item2.setState(item2.getTodoState());
        item2.setState(item2.getDoingState());

        assertEquals(2, item2.countObservers());
    }

    //#2
    @org.junit.jupiter.api.Test
    void FailBacklogItemHasObserver() {
        Person testDeveloper = new Person("Test", "test@gmail.com", Role.JUNIOR, "0612345678", "testSlack");
        Person scrumMaster = new Person("Scrummaster", "scrummaster@gmail.com", Role.SCRUMMASTER, "0611114460", "scrumMaster01");

        ProductBacklog backlog = new ProductBacklog("AvansDevOps", scrumMaster);
        SprintBacklog sprintBacklog = new SprintBacklog(backlog, "Sprint 1", scrumMaster);

        BacklogItem item = new BacklogItem(sprintBacklog, "Authentication");

        item.addObserver(testDeveloper);
        item.notifyObservers(item);

        BacklogItem item2 = new BacklogItem(sprintBacklog, "Item");
        item2.addObserver(testDeveloper);
        testDeveloper.addBacklogItem(item2);

        item2.setState(item2.getTodoState());
        item2.setState(item2.getDoingState());

        assertNotEquals(2, item2.countObservers());
    }
}
