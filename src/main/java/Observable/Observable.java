package Observable;

import java.util.ArrayList;

public class Observable {
    public ArrayList<Observer> observers = new ArrayList<Observer>();
    private boolean changed = false;

    public void addObserver(Observer o) {
        this.observers.add(o);
    }

    public int countObservers(){
        return this.observers.size();
    }

    public void deleteObserver(Observer o) {
        this.observers.remove(o);
    }

    public void pushToObservers(Object arg) {
        Object[] arrLocal;

        synchronized (this) {
            arrLocal = observers.toArray();
            clearChanged();
        }

        for (int i = arrLocal.length-1; i>=0; i--)
            ((Observer)arrLocal[i]).update(this, arg);
    }

    public void notifyObservers(Object arg) {
        Object[] arrLocal;

        if(!changed) return;
        synchronized (this) {
            arrLocal = observers.toArray();
            clearChanged();
        }

        for (int i = arrLocal.length-1; i>=0; i--)
            ((Observer)arrLocal[i]).update(this, arg);
    }

    public void setChanged() {
        this.changed = true;
    }

    public void clearChanged() {
        this.changed = false;
    }

    public boolean hasChanged() {
        return this.changed;
    }
}