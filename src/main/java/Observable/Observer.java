package Observable;

public interface Observer {
    void update(Observable o, Object object);
}
