package Notifications;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ErrorMessage {
    public static void sendErrorMessage(String message) {
        Logger logger = Logger.getLogger("Error Message");
        logger.log(Level.WARNING, message);
    }

    public static void sendStateError(String name) {
        Logger logger = Logger.getLogger("State error");
        logger.log(Level.WARNING, "Can't change to this state from " + name);
    }
}