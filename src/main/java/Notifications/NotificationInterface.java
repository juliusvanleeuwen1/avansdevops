package Notifications;

public interface NotificationInterface {
    void sendMessage(String message);
}