package BacklogItemState;

import Domain.BacklogItem;
import Notifications.ErrorMessage;

public class DoneState implements BacklogItemState {
    BacklogItem item;
    private final String NAME = "DONE";

    public DoneState(BacklogItem item) {
        this.item = item;
    }

    @Override
    public void createdBacklogItem() {
        ErrorMessage.sendErrorMessage(NAME);
    }

    @Override
    public void setToDo() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setDoing() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setReadyForTesting() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setTesting() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setTested() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setDone() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public String getNAME() {
        return NAME;
    }
}
