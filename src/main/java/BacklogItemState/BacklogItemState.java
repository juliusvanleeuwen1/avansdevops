package BacklogItemState;

public interface BacklogItemState {
    void createdBacklogItem();
    void setToDo();
    void setDoing();
    void setReadyForTesting();
    void setTesting();
    void setTested();
    void setDone();
    String getNAME();
}
