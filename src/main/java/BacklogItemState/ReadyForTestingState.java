package BacklogItemState;

import Domain.BacklogItem;
import Notifications.ErrorMessage;

public class ReadyForTestingState implements BacklogItemState {
    BacklogItem item;
    private final static String NAME = "READYFORTESTING";

    public ReadyForTestingState(BacklogItem item) {
        this.item = item;
    }

    @Override
    public void createdBacklogItem() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setToDo() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setDoing() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setReadyForTesting() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setTesting() {
        item.pushToObservers("Item set to testing");
        this.item.setState(item.getTestingState());
    }

    @Override
    public void setTested() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setDone() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public String getNAME() {
        return NAME;
    }
}