package BacklogItemState;

import MessengerAdapter.Adapters.SmsMessengerAdapter;
import MessengerAdapter.Interface.MessengerInterface;
import MessengerAdapter.Messengers.SmsMessenger;
import Domain.BacklogItem;
import Notifications.ErrorMessage;

public class TestingState implements BacklogItemState {
    BacklogItem item;
    private final String NAME = "TESTING";

    public TestingState(BacklogItem item) {
        this.item = item;
    }

    @Override
    public void createdBacklogItem() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setToDo() {
        SmsMessenger birdMessenger = new SmsMessenger();
        MessengerInterface messenger = new SmsMessengerAdapter(birdMessenger);
        messenger.sendMessage("Message to scrummaster: ", item.getSprintBacklog().getScrumMaster());
        item.pushToObservers("Item set to to do");
        this.item.setState(item.getTodoState());
    }

    @Override
    public void setDoing() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setReadyForTesting() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setTesting() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setTested() {
        item.pushToObservers("Item is tested");
        this.item.setState(item.getTestedState());
    }

    @Override
    public void setDone() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public String getNAME() {
        return NAME;
    }
}
