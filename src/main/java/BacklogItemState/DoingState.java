package BacklogItemState;

import MessengerAdapter.Adapters.SmsMessengerAdapter;
import MessengerAdapter.Interface.MessengerInterface;
import MessengerAdapter.Messengers.SmsMessenger;
import Domain.BacklogItem;
import Domain.Person;
import Notifications.ErrorMessage;

public class DoingState implements BacklogItemState{
    BacklogItem item;
    private final String NAME = "DOING";

    public DoingState(BacklogItem item) {
        this.item = item;
    }

    @Override
    public void createdBacklogItem() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setToDo() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setDoing() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setReadyForTesting() {
        item.pushToObservers("Item set to ready for testing");
        SmsMessenger birdMessenger = new SmsMessenger();
        MessengerInterface messenger = new SmsMessengerAdapter(birdMessenger);
        for (Person developer : item.getSprintBacklog().getTesters()){
            messenger.sendMessage("Item set to ready for testing", developer);
        }
        this.item.setState(item.getReadyForTestingState());
    }

    @Override
    public void setTesting() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setTested() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setDone() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public String getNAME() {
        return NAME;
    }
}
