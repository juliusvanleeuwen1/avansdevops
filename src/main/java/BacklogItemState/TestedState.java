package BacklogItemState;

import Domain.BacklogItem;
import Notifications.ErrorMessage;

public class TestedState  implements BacklogItemState {
    BacklogItem item;
    private final String NAME = "CREATED";

    public TestedState(BacklogItem item) {
        this.item = item;
    }

    @Override
    public void createdBacklogItem() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setToDo() {
        this.item.setState(item.getTodoState());
    }

    @Override
    public void setDoing() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setReadyForTesting() {
        this.item.pushToObservers("Item set to ready for testing");
        this.item.setState(item.getReadyForTestingState());
    }

    @Override
    public void setTesting() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setTested() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setDone() {
        this.item.pushToObservers("Item set to done");
        this.item.setState(item.getDoneState());
    }

    @Override
    public String getNAME() {
        return NAME;
    }
}
