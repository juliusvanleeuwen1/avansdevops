package BacklogItemState;

import Domain.BacklogItem;
import Notifications.ErrorMessage;

public class CreatedState implements BacklogItemState {
    BacklogItem item;
    private final String NAME = "CREATED";

    public CreatedState(BacklogItem item) {
        this.item = item;
    }

    @Override
    public void createdBacklogItem() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setToDo() {
        item.setChanged();
        item.pushToObservers("Item set to to do");
        item.clearChanged();
        item.setState(item.getTodoState());
    }

    @Override
    public void setDoing() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setReadyForTesting() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setTesting() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setTested() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setDone() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public String getNAME() {
        return NAME;
    }
}