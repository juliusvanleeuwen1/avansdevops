package BacklogItemState;

import Domain.BacklogItem;
import Notifications.ErrorMessage;

public class ToDoState implements BacklogItemState {
    private BacklogItem item;
    private final String NAME = "TODO";

    public ToDoState(BacklogItem item) {
        this.item = item;
    }

    @Override
    public void createdBacklogItem() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setToDo() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setDoing() {
        this.item.setState(item.getDoingState());
        item.pushToObservers("Item set to doing!");
    }

    @Override
    public void setReadyForTesting() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setTesting() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setTested() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public void setDone() {
        ErrorMessage.sendStateError(NAME);
    }

    @Override
    public String getNAME() {
        return NAME;
    }
}
