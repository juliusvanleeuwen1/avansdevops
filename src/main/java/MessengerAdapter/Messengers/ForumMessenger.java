package MessengerAdapter.Messengers;

import Domain.Person;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ForumMessenger {
    Logger logger = Logger.getLogger("Logger");

    public void sendForumMessage(String message, Person developer) {
        logger.log(Level.INFO ,"Forum: " + message + " " + developer.getEmail());
    }
}
