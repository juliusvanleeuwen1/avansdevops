package MessengerAdapter.Messengers;

import Domain.Person;

import java.util.logging.Level;
import java.util.logging.Logger;

public class EmailMessenger {
    Logger logger = Logger.getLogger("Logger");

    public void sendEmail(String message, Person developer) {
        logger.log(Level.INFO ,"Email: " + message + " " + developer.getEmail());
    }
}
