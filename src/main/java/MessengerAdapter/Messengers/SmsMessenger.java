package MessengerAdapter.Messengers;

import Domain.Person;

import java.util.logging.Level;
import java.util.logging.Logger;

public class SmsMessenger {
    Logger logger = Logger.getLogger("Logger");

    public void sendSms(String message, Person developer) {
        logger.log(Level.INFO ,"Bird: " + message + " " + developer.getEmail());
    }
}