package MessengerAdapter.Messengers;

import Domain.Person;

import java.util.logging.Level;
import java.util.logging.Logger;

public class SlackMessenger {
    Logger logger = Logger.getLogger("Logger");

    public void sendSlackMessage(String message, Person developer) {
        logger.log(Level.INFO ,"Slack: " + message + " " + developer.getEmail());
    }
}
