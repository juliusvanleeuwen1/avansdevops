package MessengerAdapter.Adapters;

import MessengerAdapter.Interface.MessengerInterface;
import MessengerAdapter.Messengers.SmsMessenger;
import Domain.Person;

public class SmsMessengerAdapter implements MessengerInterface {
    private final SmsMessenger smsMessenger;

    public SmsMessengerAdapter(SmsMessenger smsMessenger) {
        this.smsMessenger = smsMessenger;
    }

    @Override
    public void sendMessage(String message, Person developer) {
        this.smsMessenger.sendSms(message, developer);
    }
}