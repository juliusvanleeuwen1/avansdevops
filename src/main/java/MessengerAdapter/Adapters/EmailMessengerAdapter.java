package MessengerAdapter.Adapters;

import MessengerAdapter.Interface.MessengerInterface;
import MessengerAdapter.Messengers.EmailMessenger;
import Domain.Person;

public class EmailMessengerAdapter implements MessengerInterface {
    private final EmailMessenger emailMessenger;

    public EmailMessengerAdapter(EmailMessenger emailMessenger) {
        this.emailMessenger = emailMessenger;
    }

    @Override
    public void sendMessage(String message, Person developer) {
        this.emailMessenger.sendEmail(message, developer);
    }
}