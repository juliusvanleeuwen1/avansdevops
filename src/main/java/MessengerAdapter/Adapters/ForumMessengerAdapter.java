package MessengerAdapter.Adapters;

import Domain.Person;
import MessengerAdapter.Interface.MessengerInterface;
import MessengerAdapter.Messengers.ForumMessenger;

public class ForumMessengerAdapter implements MessengerInterface {
    private final ForumMessenger forumMessenger;

    public ForumMessengerAdapter(ForumMessenger forumMessenger) {
        this.forumMessenger = forumMessenger;
    }

    @Override
    public void sendMessage(String message, Person developer) {
        this.forumMessenger.sendForumMessage(message, developer);
    }
}
