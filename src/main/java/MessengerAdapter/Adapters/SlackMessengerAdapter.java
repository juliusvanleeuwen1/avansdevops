package MessengerAdapter.Adapters;

import MessengerAdapter.Interface.MessengerInterface;
import MessengerAdapter.Messengers.SlackMessenger;
import Domain.Person;

public class SlackMessengerAdapter implements MessengerInterface {
    private final SlackMessenger slackMessenger;

    public SlackMessengerAdapter(SlackMessenger slackMessenger) {
        this.slackMessenger = slackMessenger;
    }

    @Override
    public void sendMessage(String message, Person developer) {
        this.slackMessenger.sendSlackMessage(message, developer);
    }
}
