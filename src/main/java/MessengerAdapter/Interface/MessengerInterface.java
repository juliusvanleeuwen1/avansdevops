package MessengerAdapter.Interface;

import Domain.Person;

public interface MessengerInterface {
    void sendMessage(String message, Person developer);
}
