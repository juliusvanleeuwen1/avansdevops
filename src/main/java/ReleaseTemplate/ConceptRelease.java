package ReleaseTemplate;

import Domain.SprintBacklog;
import MessengerAdapter.Adapters.EmailMessengerAdapter;
import MessengerAdapter.Interface.MessengerInterface;
import MessengerAdapter.Messengers.EmailMessenger;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConceptRelease  extends Release {
    Logger logger = Logger.getLogger("Logger");

    private SprintBacklog sprintBacklog;
    private boolean FinishedSuccessful;
    public ConceptRelease(SprintBacklog sprintBacklog) {
        this.sprintBacklog = sprintBacklog;
    }

    @Override
    public void build() {
        try {
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Building concept environment..0%");
            TimeUnit.SECONDS.sleep(2);
            logger.log(Level.INFO,"Building concept environment.......60%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Building concept environment..........100%");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void test() {
        try {
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Testing concept environment..5%");
            TimeUnit.SECONDS.sleep(2);
            logger.log(Level.INFO,"Testing concept environment.......45%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Testing concept environment..........100%");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void deploy() {
        try {
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Deploying concept environment..0%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Deploying concept environment...20%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Deploying concept environment...28%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Deploying concept environment.......50%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Deploying concept environment..........100%");
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void finished() {
        this.FinishedSuccessful = true;
        EmailMessenger emailMessenger = new EmailMessenger();
        MessengerInterface messenger = new EmailMessengerAdapter(emailMessenger);
        messenger.sendMessage("Concept release pipeline succeeded", sprintBacklog.getScrumMaster());
        messenger.sendMessage("Concept release pipeline succeeded", sprintBacklog.getProductBacklog().getProductOwner());
    }

    public boolean isFinishedSuccessful() {
        return FinishedSuccessful;
    }
}