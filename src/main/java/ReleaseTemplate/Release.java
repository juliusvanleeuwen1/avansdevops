package ReleaseTemplate;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Release {
    Logger logger = Logger.getLogger("Logger");

    private boolean FinishedSuccessful;
    final public void release() {
        this.build();
        this.test();
        this.deploy();
        this.finished();
    }

    public void build() {
        logger.log(Level.INFO,"building");
    }

    public void test() {
        logger.log(Level.INFO,"tested");
    }

    public void deploy() {
        logger.log(Level.INFO,"deployed");
    }

    public void finished() {

    }
    public boolean isFinishedSuccessful(){
        return FinishedSuccessful;
    }
}