package ReleaseTemplate;

import MessengerAdapter.Adapters.EmailMessengerAdapter;
import MessengerAdapter.Interface.MessengerInterface;
import MessengerAdapter.Messengers.EmailMessenger;
import Domain.SprintBacklog;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainRelease extends Release {
    private SprintBacklog sprintBacklog;
    private boolean FinishedSuccessful;
    public MainRelease(SprintBacklog sprintBacklog) {
        this.sprintBacklog = sprintBacklog;
    }
    Logger logger = Logger.getLogger("Logger");

    @Override
    public void build() {
        try {
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Building main environment..0%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Building main environment...15%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Building main environment.......45%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Building main environment.......85%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Building main environment..........100%");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void test() {
        try {
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Testing main environment..0%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Testing main environment.....20%");
            TimeUnit.SECONDS.sleep(3);
            logger.log(Level.INFO,"Testing main environment..........100%");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void deploy() {
        try {
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Deploying main environment..0%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Deploying main environment...20%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Deploying main environment.......60%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Deploying main environment..........100%");
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void finished() {
        this.FinishedSuccessful = true;
        EmailMessenger emailMessenger = new EmailMessenger();
        MessengerInterface messenger = new EmailMessengerAdapter(emailMessenger);
        messenger.sendMessage("Main release pipeline succeeded", sprintBacklog.getScrumMaster());
        messenger.sendMessage("Main release pipeline succeeded", sprintBacklog.getProductBacklog().getProductOwner());
    }

    public boolean isFinishedSuccessful() {
        return FinishedSuccessful;
    }
}
