package ReleaseTemplate;

import MessengerAdapter.Adapters.EmailMessengerAdapter;
import MessengerAdapter.Interface.MessengerInterface;
import MessengerAdapter.Messengers.EmailMessenger;
import Domain.SprintBacklog;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StagingRelease extends Release {
    private SprintBacklog sprintBacklog;
    private boolean FinishedSuccessful;
    public StagingRelease(SprintBacklog sprintBacklog) {
        this.sprintBacklog = sprintBacklog;
    }
    Logger logger = Logger.getLogger("Logger");

    @Override
    public void build() {
        try {
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Building staging environment..0%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Building staging environment...10%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Building staging environment...40%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Building staging environment.......70%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Building staging environment..........100%");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void test() {
        try {
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Testing staging environment..0%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Testing staging environment...20%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Testing staging environment..........100%");
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void deploy() {
        try {
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Deploying staging environment..0%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Deploying staging environment...20%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Deploying staging environment.......60%");
            TimeUnit.SECONDS.sleep(1);
            logger.log(Level.INFO,"Deploying staging environment..........100%");
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void finished() {
        this.FinishedSuccessful = true;
        EmailMessenger emailMessenger = new EmailMessenger();
        MessengerInterface messenger = new EmailMessengerAdapter(emailMessenger);
        messenger.sendMessage("Staging release pipeline succeeded", sprintBacklog.getScrumMaster());
        messenger.sendMessage("Staging release pipeline succeeded", sprintBacklog.getProductBacklog().getProductOwner());
    }

    public boolean isFinishedSuccessful() {
        return FinishedSuccessful;
    }
}
