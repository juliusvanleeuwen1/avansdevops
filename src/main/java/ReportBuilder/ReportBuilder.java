package ReportBuilder;

public interface ReportBuilder {
    void activateHeader();
    void activateFooter();
    Report getReport();
}
