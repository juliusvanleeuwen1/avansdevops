package ReportBuilder;

public class Director {
    private ReportBuilder reportBuilder;

    public Director(ReportBuilder reportBuilder)
    {
        this.reportBuilder = reportBuilder;
    }

    public Report getReport()
    {
        return this.reportBuilder.getReport();
    }

    public void createReport()
    {
        this.reportBuilder.activateFooter();
        this.reportBuilder.activateHeader();
    }

}
