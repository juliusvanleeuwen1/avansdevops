package ReportBuilder;

import Domain.Person;
import Domain.SprintBacklog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Report {
    Random random = new Random();
    //Header
    private String businessName;
    private String projectName;
    private String version;
    private Date date;

    public String header;
    public String footer;
    public boolean showHeader = false;
    public boolean showFooter = false;
    public float effortPoints;
    private SprintBacklog sprintBacklog;
    Logger logger = Logger.getLogger("Logger");

    public Report(SprintBacklog sprintBacklog) {
        this.sprintBacklog = sprintBacklog;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public void activateFooter() {
        this.showFooter = true;
    }

    public void activateHeader() {
        this.showHeader = true;
    }

    public void disableHeader() {
        this.showHeader = false;
    }

    public void disableFooter() {
        this.showFooter = false;
    }

    public List<Person> getTeam() {
        List<Person> team = new ArrayList<Person>();
        var items = this.sprintBacklog.getBacklogItems();
        for(var i : items) {
            var developer = i.getDeveloper();
            team.add(developer);
        }

        return team;
    }

    public void printEffortPoints() {
        float points = 0;
        var items = this.sprintBacklog.getBacklogItems();
        for(var i : items) {
            var developer = i.getDeveloper();
            logger.log(Level.INFO, "");
            logger.log(Level.INFO,developer.getEmail() + " Effort points per task: " + developer.getEffortPoints());
            logger.log(Level.INFO,developer.getEmail() + " total tasks: " + developer.getBacklogItems().size());
            logger.log(Level.INFO,"total effort: " + developer.getBacklogItems().size() * developer.getEffortPoints());
            points += developer.getBacklogItems().size() * developer.getEffortPoints();
        }
        this.effortPoints = points;
        logger.log(Level.INFO,"");
        logger.log(Level.INFO,points + " total effortpoints");
    }

    public void printCharts() {
        var percentage = random.nextInt(100);

        logger.log(Level.INFO, "Charts " +
                "These are charts for your team:" +
                "Stats:" + percentage + " done." +
                "Charts: Bar chart of all tasks done");
    }

    public List<Person> printTeam() {
        List<Person> team = new ArrayList<Person>();
        var items = this.sprintBacklog.getBacklogItems();
        for(var i : items) {
            var developer = i.getDeveloper();
            logger.log(Level.INFO,developer.getName() + ", effortpoints: " + developer.getEffortPoints());
        }

        return team;
    }

    public void generateEffortPointsReport() {
        if (showHeader) {
            logger.log(Level.INFO,"HEADER");
        }

        this.printEffortPoints();

        if (showFooter) {
            logger.log(Level.INFO,"FOOTER");
        }
    }

    public void generateTeamsReport() {
        if (showHeader) {
            logger.log(Level.INFO, "HEADER");
        }

        this.printTeam();


        if (showFooter) {
            logger.log(Level.INFO, "Footer");
        }
    }

    public void generateCharts() {
        if (showHeader) {
            logger.log(Level.INFO, "HEADER");
        }

        this.printCharts();


        if (showFooter) {
            logger.log(Level.INFO, "Footer");
        }
    }

    public float getEffortPoints() {
        return effortPoints;
    }
}
