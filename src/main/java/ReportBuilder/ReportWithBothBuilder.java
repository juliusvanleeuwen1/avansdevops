package ReportBuilder;

import Domain.SprintBacklog;

public class ReportWithBothBuilder implements ReportBuilder {
    private Report report;

    public ReportWithBothBuilder(SprintBacklog sprintBacklog) {
        this.report = new Report(sprintBacklog);
    }

    @Override
    public void activateHeader() {
        this.report.activateHeader();
    }

    @Override
    public void activateFooter() {
        this.report.activateFooter();
    }

    @Override
    public Report getReport() {
        return this.report;
    }
}
