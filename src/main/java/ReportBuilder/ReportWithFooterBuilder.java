package ReportBuilder;

import Domain.SprintBacklog;

public class ReportWithFooterBuilder implements ReportBuilder {
    private Report report;

    public ReportWithFooterBuilder(SprintBacklog sprintBacklog) {
        this.report = new Report(sprintBacklog);
    }

    @Override
    public void activateHeader() {
        this.report.disableHeader();
    }

    @Override
    public void activateFooter() {
        this.report.activateFooter();
    }

    @Override
    public Report getReport() {
        return this.report;
    }
}
