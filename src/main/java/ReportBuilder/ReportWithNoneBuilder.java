package ReportBuilder;

import Domain.SprintBacklog;

public class ReportWithNoneBuilder implements ReportBuilder {
    private Report report;

    public ReportWithNoneBuilder(SprintBacklog sprintBacklog) {
        this.report = new Report(sprintBacklog);
    }

    @Override
    public void activateHeader() {
        this.report.disableHeader();
    }

    @Override
    public void activateFooter() {
        this.report.disableFooter();
    }

    @Override
    public Report getReport() {
        return this.report;
    }
}
