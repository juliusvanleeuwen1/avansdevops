package ReportBuilder;

import Domain.SprintBacklog;

public class ReportWithHeaderBuilder implements ReportBuilder {
    private Report report;

    public ReportWithHeaderBuilder(SprintBacklog sprintBacklog) {
        this.report = new Report(sprintBacklog);
    }

    @Override
    public void activateHeader() {
        this.report.activateHeader();
    }

    @Override
    public void activateFooter() {
        this.report.disableFooter();
    }


    @Override
    public Report getReport() {
        return this.report;
    }
}
