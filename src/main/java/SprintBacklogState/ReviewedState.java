package SprintBacklogState;

import MessengerAdapter.Adapters.SmsMessengerAdapter;
import MessengerAdapter.Interface.MessengerInterface;
import MessengerAdapter.Messengers.SmsMessenger;
import Domain.SprintBacklog;
import Notifications.ErrorMessage;

public class ReviewedState implements BacklogState {
    SprintBacklog backlog;
    private final static String name = "Reviewed state";

    public ReviewedState(SprintBacklog sprintBacklog) {
        this.backlog = sprintBacklog;
    }

    @Override
    public void setInDevelopment() {
        SmsMessenger birdMessenger = new SmsMessenger();
        MessengerInterface messenger = new SmsMessengerAdapter(birdMessenger);
        messenger.sendMessage("Sprint is moved to development", backlog.getScrumMaster());
        this.backlog.setState(backlog.getInDevelopmentState());
    }

    @Override
    public void setReviewed() {
        ErrorMessage.sendStateError(name);
    }

    @Override
    public void setFinished() {
        SmsMessenger birdMessenger = new SmsMessenger();
        MessengerInterface messenger = new SmsMessengerAdapter(birdMessenger);
        messenger.sendMessage("Sprint is finished", backlog.getScrumMaster());
        this.backlog.setState(backlog.getFinishedState());
        this.backlog.setDone();
    }

    @Override
    public void setCreated() {
        ErrorMessage.sendStateError(name);
    }

    @Override
    public String getName() {
        return name;
    }
}