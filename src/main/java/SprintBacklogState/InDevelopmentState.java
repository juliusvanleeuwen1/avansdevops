package SprintBacklogState;

import Domain.SprintBacklog;
import Notifications.ErrorMessage;

public class InDevelopmentState implements BacklogState {
    SprintBacklog backlog;
    private final static String name = "In development state";

    public InDevelopmentState(SprintBacklog sprintBacklog) {
        this.backlog = sprintBacklog;
    }

    @Override
    public void setInDevelopment() {
        ErrorMessage.sendStateError(name);
    }

    @Override
    public void setReviewed() {
        ErrorMessage.sendStateError(name);
    }

    @Override
    public void setFinished() {
        ErrorMessage.sendStateError(name);
    }

    @Override
    public void setCreated() {
        ErrorMessage.sendStateError(name);
    }

    @Override
    public String getName() {
        return name;
    }
}