package SprintBacklogState;

import Domain.SprintBacklog;
import Notifications.ErrorMessage;

public class CreatedState implements BacklogState {
    SprintBacklog backlog;
    private final static String name = "Created state";

    public CreatedState(SprintBacklog sprintBacklog) {
        this.backlog = sprintBacklog;
    }

    @Override
    public void setInDevelopment() {
        this.backlog.setState(backlog.getInDevelopmentState());
    }

    @Override
    public void setReviewed() {
        ErrorMessage.sendStateError(name);
    }

    @Override
    public void setFinished() {
        ErrorMessage.sendStateError(name);
    }

    @Override
    public void setCreated() {
        ErrorMessage.sendStateError(name);
    }

    @Override
    public String getName() {
        return name;
    }
}