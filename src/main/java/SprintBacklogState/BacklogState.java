package SprintBacklogState;

public interface BacklogState {
    void setInDevelopment();
    void setReviewed();
    void setFinished();
    void setCreated();
    String getName();
}
