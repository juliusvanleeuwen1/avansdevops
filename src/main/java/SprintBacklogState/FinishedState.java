package SprintBacklogState;

import Domain.SprintBacklog;
import Notifications.ErrorMessage;

public class FinishedState implements BacklogState {
    SprintBacklog backlog;
    private final static String name =  "Finished state";

    public FinishedState(SprintBacklog sprintBacklog) {
        this.backlog = sprintBacklog;
    }

    @Override
    public void setInDevelopment() {
        ErrorMessage.sendStateError(name);
    }

    @Override
    public void setReviewed() {
        ErrorMessage.sendStateError(name);
    }

    @Override
    public void setFinished() {
        ErrorMessage.sendStateError(name);
    }

    @Override
    public void setCreated() {
        ErrorMessage.sendStateError(name);
    }


    @Override
    public String getName() {
        return name;
    }
}