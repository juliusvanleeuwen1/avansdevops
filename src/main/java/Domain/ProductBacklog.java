package Domain;

import java.util.ArrayList;
import java.util.List;

public class ProductBacklog {
    public List<BacklogItem> items = new ArrayList<>();
    public String name;
    public Person productOwner;

    public ProductBacklog(String name, Person productOwner) {
        this.name = name;
        this.productOwner = productOwner;
    }

    public Person getProductOwner() {
        return productOwner;
    }

    public void addItem(BacklogItem item) {
        this.items.add(item);
    }

    public String getName() {
        return name;
    }

    public List<BacklogItem> getItems() {
        return items;
    }
}