package Domain;

public class Activity {
    public boolean done = false;
    public String subject;
    public BacklogItem item;
    public Person developer;

    public Activity(BacklogItem item, String subject) {
        this.item = item;
        this.subject = subject;
        this.addActivity(item);
    }

    public void addActivity(BacklogItem item) {
        item.addActivity(this);
    }

    public void removeActivity(BacklogItem item) {
        item.removeActivity(this);
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean isDone() {
        return done;
    }

    public Person getDeveloper() {
        return developer;
    }

    public void setDeveloper(Person developer) {
        this.developer = developer;
        developer.addActivity(this);
    }
}