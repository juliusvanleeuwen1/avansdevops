package Domain;

import BacklogItemState.*;
import Observable.Observable;

import java.util.ArrayList;
import java.util.List;

public class BacklogItem extends Observable {
    private SprintBacklog sprintBacklog;
    private boolean done = false;
    private Person developer;
    private Person tester;
    private ArrayList<Activity> activities = new ArrayList<>();
    private ArrayList<Discussion> discussions = new ArrayList<>();

    private String subject;

    private BacklogItemState createdState;
    private BacklogItemState todoState;
    private BacklogItemState doingState;
    private BacklogItemState readyForTestingState;
    private BacklogItemState testingState;
    private BacklogItemState testedState;
    private BacklogItemState doneState;

    BacklogItemState state;

    public BacklogItem(SprintBacklog sprintBacklog, String subject){
        this.createdState = new CreatedState(this);
        this.todoState = new ToDoState(this);
        this.doingState = new DoingState(this);
        this.readyForTestingState = new ReadyForTestingState(this);
        this.testingState = new TestingState(this);
        this.testedState = new TestedState(this);
        this.doneState = new DoneState(this);
        this.subject = subject;

        this.state = createdState;
        sprintBacklog.addBacklogItems(this);
    }


    public void setSprintBacklog(SprintBacklog sprintBacklog) {
        this.sprintBacklog = sprintBacklog;
    }

    public void addActivity(Activity activity){
            this.activities.add(activity);
    }

    public void removeActivity(Activity activity){
            this.activities.remove(activity);
    }

    public void addDiscussion(Discussion discussion) {
            this.discussions.add(discussion);
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setState(BacklogItemState state) {
            this.state = state;
    }

    public BacklogItemState getState() {
        return this.state;
    }

    public void setDeveloper(Person developer) {
        this.developer = developer;
        developer.addBacklogItem(this);
    }

    public void removeDeveloper() {
        this.developer = null;
    }

    public void setDone(Person person) {
        if(person.getRole().equals(Role.LEAD)) {
            for (var i : activities) {
                if (!i.isDone()) {
                    this.done = false;
                    return;
                }
            }
            this.state = this.getDoneState();
            this.done = true;
        }
    }

    public void setTester(Person tester) {
        this.tester = tester;
    }

    public void setNotDone() {
        this.done = false;
    }

    public boolean getDone() {
        return this.done;
    }

    public String getSubject() {
        return subject;
    }

    public Person getDeveloper() {
        return developer;
    }

    public BacklogItemState getCreatedState() {
        return createdState;
    }

    public BacklogItemState getTodoState() {
        return todoState;
    }

    public BacklogItemState getDoingState() {
        return doingState;
    }

    public BacklogItemState getReadyForTestingState() {
        return readyForTestingState;
    }

    public BacklogItemState getTestingState() {
        return testingState;
    }

    public BacklogItemState getTestedState() {
        return testedState;
    }

    public BacklogItemState getDoneState() {
        return doneState;
    }

    public SprintBacklog getSprintBacklog() {
        return sprintBacklog;
    }
}