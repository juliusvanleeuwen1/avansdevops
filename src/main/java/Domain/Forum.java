package Domain;

import java.util.ArrayList;
import java.util.List;

public class Forum {
    public String name;
    public List<Discussion> discussions = new ArrayList<>();

    public Forum(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addDiscussion(Discussion discussion) {
        this.discussions.add(discussion);
    }

    public List<Discussion> getDiscussions() {
        return discussions;
    }

    public void printDiscussions() {
        for (var i : discussions) {
            var count = 1;
            for (var x : i.getReactions()) {
                System.out.println("Onderwerp: " + i.getSubject() + ": Reactie " + count + ": " + x.getDescription());
                count++;
            }
        }
    }
}