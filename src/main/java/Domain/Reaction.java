package Domain;

public class Reaction {
    public String description;
    private Discussion discussion;

    public Reaction(String reaction, Discussion discussion) {
        this.description = reaction;
        this.discussion = discussion;
    }

    public String getDescription() {
        return description;
    }

    public Discussion getDiscussion() {
        return discussion;
    }
}
