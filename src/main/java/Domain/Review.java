package Domain;

public class Review {
    private Stakeholder stakeholder;
    private String description;

    public Review(Stakeholder stakeholder, String description) {
        this.stakeholder = stakeholder;
        this.description = description;
    }

    public Stakeholder getStakeholder() {
        return stakeholder;
    }

    public String getDescription() {
        return description;
    }
}
