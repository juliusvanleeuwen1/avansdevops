package Domain;

public class SprintSummary {
    private String summary;
    private SprintBacklog sprintBacklog;

    public SprintSummary(SprintBacklog sprintBacklog) {
        this.sprintBacklog = sprintBacklog;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}