package Domain;

import Observable.Observable;
import Observable.Observer;

import java.util.ArrayList;
import java.util.List;

public class Person implements Observer {
    private String name;
    private String email;
    private String phoneNumber;
    private String slackAccount;
    private double effortPoints;

    private Role role;

    private List<BacklogItem> backlogItems = new ArrayList<>();
    private List<Activity> activities = new ArrayList<>();

    public Person(String name, String mail, Role role, String phone, String slack) {
        this.name = name;
        this.email = mail;
        this.role = role;
        this.phoneNumber = phone;
        this.slackAccount = slack;

        if(this.role.equals(Role.JUNIOR)) {
            this.effortPoints = 0.8;
        } else if (this.role.equals(Role.SENIOR)) {
            this.effortPoints = 1.2;
        } else {
            this.effortPoints = 1;
        }
    }

    public void setItemDone(BacklogItem item) {
        if (this.role.equals(Role.LEAD)) {
            item.setDone(this);
            item.setState(item.getDoneState());
            return;
        }

        System.out.println("You have to be Lead developer to set item to done.");
    }

    public void createBacklogSummary(SprintBacklog backlog, String summary) {
        if(this.role.equals(Role.SCRUMMASTER)) {
            backlog.setSummary(this, summary);
        }
    }

    public double getEffortPoints() {
        return effortPoints;
    }

    public void addBacklogItem(BacklogItem item) {
        this.backlogItems.add(item);
    }

    public void addActivity(Activity activity) {
        this.activities.add(activity);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getSlackAccount() {
        return slackAccount;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public List<BacklogItem> getBacklogItems() {
        return backlogItems;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public void update(Observable o, Object object) {
        System.out.println(object);
    }
}