package Domain;

import ReleaseTemplate.Release;
import ReportBuilder.Report;
import SprintBacklogState.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SprintBacklog {
    private ProductBacklog productBacklog;
    private Date deadline;
    private ArrayList<BacklogItem> backlogItems = new ArrayList<>();
    private SprintReview sprintReview;
    private Person scrumMaster;
    private ArrayList<Person> testers = new ArrayList<>();
    private SprintSummary summary;

    private boolean done;

    public BacklogState createdState;
    public BacklogState inDevelopmentState;
    public BacklogState reviewedState;
    public BacklogState finishedState;

    private Report report;

    private Release release;

    BacklogState state;

    public void setDone() {
        for (var item : backlogItems) {
            if (!item.getDone()) {
                this.done = false;
                System.out.println("All backlogitems must be done in order to complete a sprint");
                return;
            }
        }
        this.done = true;
    }

    public boolean getDone() {
        return this.done;
    }

    public void startRelease(Release release) {
        if (done) {
            this.release = release;
            release.release();
        }
        System.out.println("Sprint has to be done.");
    }

    public SprintReview startReview() {
        if(done) {
            this.sprintReview = new SprintReview(this);
            return sprintReview;
        }
        return null;
    }

    public SprintBacklog(ProductBacklog productBacklog, String subject, Person scrumMaster){
        this.productBacklog = productBacklog;
        this.createdState = new CreatedState(this);
        this.inDevelopmentState = new InDevelopmentState(this);
        this.reviewedState = new ReviewedState(this);
        this.finishedState = new FinishedState(this);
        this.scrumMaster = scrumMaster;

        this.state = createdState;
    }

    public void setSummary(Person person, String summary) {
        if(person.getRole().equals(Role.SCRUMMASTER)) {
            this.summary.setSummary(summary);
        }
    }

    public ProductBacklog getProductBacklog() {
        return productBacklog;
    }

    public Report getReport() {
        return report;
    }

    public BacklogState getState() {
        return state;
    }

    public BacklogState getCreatedState() {
        return createdState;
    }

    public BacklogState getInDevelopmentState() {
        return inDevelopmentState;
    }

    public BacklogState getReviewedState() {
        return reviewedState;
    }

    public BacklogState getFinishedState() {
        return finishedState;
    }

    public void setState(BacklogState state) {
        this.state = state;
    }

    public SprintBacklog(Date deadline){
        this.deadline = deadline;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public List<BacklogItem> getBacklogItems() {
        return backlogItems;
    }

    public void addBacklogItems(BacklogItem backlogItem) {
        if (!done) {
            this.backlogItems.add(backlogItem);
        }
    }

    public Person getScrumMaster() {
        return scrumMaster;
    }

    public void setScrumMaster(Person scrumMaster) {
        this.scrumMaster = scrumMaster;
    }

    public List<Person> getTesters() {
        return testers;
    }

    public void addTester(Person developer) {
        this.testers.add(developer);
    }
}
