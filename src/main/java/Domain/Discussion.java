package Domain;

import MessengerAdapter.Adapters.SmsMessengerAdapter;
import MessengerAdapter.Interface.MessengerInterface;
import MessengerAdapter.Messengers.SmsMessenger;

import java.util.ArrayList;
import java.util.List;

public class Discussion {
    private List<Reaction> reactions = new ArrayList<>();
    private String subject;
    private Forum forum;
    private BacklogItem backlogItem;

    SmsMessenger birdMessenger = new SmsMessenger();
    MessengerInterface messenger = new SmsMessengerAdapter(birdMessenger);

    public Discussion(String subject, BacklogItem backlogItem, Forum forum) {
        this.subject = subject;
        this.backlogItem = backlogItem;
        this.forum = forum;
        forum.addDiscussion(this);
    }

    public void addReaction(Reaction reaction) {
        if(!backlogItem.getDone()) {
            this.reactions.add(reaction);
            this.messenger.sendMessage("Someone added a reaction to the discussing involving your backlogitem", backlogItem.getDeveloper());
        }
    }

    public void setSubject(String subject) {
        if(!backlogItem.getDone()) {
            this.subject = subject;
        }
    }

    public String getSubject() {
        return subject;
    }

    public List<Reaction> getReactions() {
        return reactions;
    }
}