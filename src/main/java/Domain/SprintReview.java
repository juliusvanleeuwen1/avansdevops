package Domain;

import java.util.ArrayList;
import java.util.List;

public class SprintReview {
    private SprintBacklog sprintBacklog;
    private List<Review> reviews = new ArrayList<>();
    private List<Stakeholder> stakeholders = new ArrayList<>();

    public SprintReview(SprintBacklog sprintBacklog) {
        this.sprintBacklog = sprintBacklog;
    }

    public void addReview(Review review) {
        this.reviews.add(review);
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void addStakeholder(Stakeholder stakeholder){
        this.stakeholders.add(stakeholder);
    }

    public List<Stakeholder> getStakeholders() {
        return stakeholders;
    }
}
