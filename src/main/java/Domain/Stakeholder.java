package Domain;

public class Stakeholder {
    private String name;
    private String email;

    public Stakeholder(String name, String email){
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }
}
